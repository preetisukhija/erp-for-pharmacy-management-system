/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import java.time.LocalDate;

/**
 * FXML Controller class
 *
 * @author preeti
 */
import java.time.LocalDate;
public class AddProductsController implements Initializable {

    @FXML
    private AnchorPane scrollablePane;

    @FXML
    private Pane pane;

    @FXML
    private TextField quantitytxt;

    @FXML
    private TextField productname;

    @FXML
    private Button submit;

    @FXML
    private TextField eoqleveltxt;

    @FXML
    private ComboBox<String> suppliercombo;

    @FXML
    private TextField dangerleveltxt;

    @FXML
    private ComboBox<String> categorycombo;

    @FXML
    private ComboBox<String> hsn_codeCombo;

    @FXML
    private DatePicker datepicker;
   
    @FXML
    private TextField sellingratetxt;

    ValidationSupport validationsupport = new ValidationSupport();
@Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        helper helper = new helper();
        categorycombo.setValue("Choose Category:");
        suppliercombo.setValue("Choose Suppliers:");
        hsn_codeCombo.setValue("Choose HSN Code:");
        
//        HashMap<String, String> categoryList = new HashMap<>();
//        categoryList = DbConnect.getCategory();
//        helper.setDataInCombobox(category, categoryList);

        HashMap<String, String> categoryList = new HashMap<>();
        categoryList = DbConnect.getCategories();
        helper.setDataFromArraylistToCombobox(categorycombo, categoryList);
        
        HashMap<String, String> HSNCodeList = new HashMap<>();
        HSNCodeList = DbConnect.getHsn_code();
        helper.setDataFromArraylistToCombobox(hsn_codeCombo, HSNCodeList);
        
        validationsupport.registerValidator(productname, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(sellingratetxt, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(quantitytxt, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(dangerleveltxt, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(eoqleveltxt, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(datepicker, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(suppliercombo, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(categorycombo, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(hsn_codeCombo, Validator.createEmptyValidator("Text is required")); 
    }   
    
    public void getSupplierList(){
        helper helper = new helper();
        HashMap<String, String> supplierList = new HashMap<>();
        String selectedCategory = categorycombo.getSelectionModel().getSelectedItem();
        String[] words=selectedCategory.split("\\s");
        String Selectedcate = words[0];
        supplierList = DbConnect.getSuppliers(Selectedcate);
        helper.setDataFromArraylistToCombobox(suppliercombo, supplierList);
    }
    
    public void display(){
      submit.setOnAction(e->{
          
              Connection conn = DbConnect.getConnection();
              String selectedCategory = categorycombo.getSelectionModel().getSelectedItem();
              String[] words=selectedCategory.split("\\s");
              int categoryId = Integer.parseInt(words[0]);
              String  hsnCode = hsn_codeCombo.getSelectionModel().getSelectedItem();
              String[] selectedHsn=hsnCode.split("\\s");
              int HSNCode = Integer.parseInt(selectedHsn[1]);
              int prod_id=0;
              
              String sql = "INSERT into products (name, specification, hsn_code, category_id, eoq_level, "
                      + "danger_level, quantity)" + "values(?,?,?,?,?,?,?)";
              String sql1 = "SELECT id FROM products";
              

         try {
              PreparedStatement ps = conn.prepareStatement(sql);
//             java.sql.Date sqlDate = new java.sql.Date(datepicker.getTime());
              ps.setString(1,productname.getText());
//              ps.setString(2,specifications.getText());
              ps.setInt(2,HSNCode);
              ps.setInt(3,categoryId);
              ps.setInt(4,Integer.parseInt(eoqleveltxt.getText()));
              ps.setInt(5,Integer.parseInt(dangerleveltxt.getText()));
              ps.setInt(6,Integer.parseInt(quantitytxt.getText()));
              ps.setString(7,datepicker.getValue()+"");
              ps.execute();
              
              PreparedStatement ps1 = conn.prepareStatement(sql1);
              ResultSet rs = ps1.executeQuery();
              rs.last();
              
              prod_id = rs.getInt("id");
              String sql2 = "INSERT INTO products_selling_rate (product_id,selling_rate) values(?,?)";
              String sql3 = "INSERT INTO product_supplier(product_id, supplier_id) values(?,?)";
              String sql4 = "INSERT INTO purchases ";
              
              PreparedStatement ps2 = conn.prepareStatement(sql2);
              ps2.setInt(1,prod_id);
              ps2.setInt(2,Integer.parseInt(sellingratetxt.getText()));
              ps2.execute();
              
              String selectedSupplier = suppliercombo.getSelectionModel().getSelectedItem();
              String[] selectedSuppliers= selectedSupplier.split("\\s");
              String s2 = selectedSuppliers[0];
             
              PreparedStatement ps3 = conn.prepareStatement(sql3);
              ps3.setInt(1,prod_id);
              ps3.setInt(2,Integer.parseInt(s2));
              ps3.execute();
              
            }catch (SQLException ex) {
              Logger.getLogger(AddProductsController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
     }
}
