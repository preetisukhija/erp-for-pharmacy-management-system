import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class AddPurchaseController implements Initializable {
    
    @FXML
    private ComboBox<String> products;

    @FXML
    private ComboBox<String> category;

    @FXML
    private TextField purchaserate;

    @FXML
    private TextField quantity;

    @FXML
    private ComboBox<String> suppliers;
    
    private String selectedCategory;
    
    private String  selectedProduct;
    
    private String selectedSupplier;
 
    private int purchaseRate;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        category.setValue("Choose Category:");
        suppliers.setValue("Choose Suppliers:");
        products.setValue("Choose Products:");
        helper helper = new helper();
        HashMap categoryList = new HashMap();
        categoryList = DbConnect.getCategories();
        helper.setDataFromArraylistToCombobox(category, categoryList);
    }
    
    public void stateChangedAll() {
        
        helper helper = new helper();
        HashMap<String, String> supplierList = new HashMap<>();
        selectedCategory =  category.getSelectionModel().getSelectedItem();
        selectedProduct = products.getSelectionModel().getSelectedItem();
        
        String[] selectedCat= selectedCategory.split("\\s");
        String s = selectedCat[0];
        String[] selectedProd= selectedProduct.split("\\s");
        String s1 = selectedProd[0];
        supplierList = DbConnect.getSuppliersAccordingToCategory(s, s1);
        System.out.println(supplierList);
        helper.setDataFromArraylistToCombobox(suppliers, supplierList);
    }
    
   public void stateChanged() {
        helper helper = new helper();
        selectedCategory = category.getSelectionModel().getSelectedItem();
        String[] selectedCat= selectedCategory.split("\\s");
        String s = selectedCat[0];
        HashMap prod = DbConnect.getProductsAccorgingToCategory(s);
        helper.setDataFromArraylistToCombobox(products, prod);
    }
    
    public void stateChangedOfSupplier(){
        
        selectedCategory =  category.getSelectionModel().getSelectedItem();
        selectedProduct = products.getSelectionModel().getSelectedItem();
        String[] selectedCat= selectedCategory.split("\\s");
        String s = selectedCat[0];
        String[] selectedProd= selectedProduct.split("\\s");
        String s1 = selectedProd[0];
        purchaseRate = DbConnect.getPurchaseRate(s1,s);
        
        purchaserate.setText(purchaseRate+"");
    }
    
    public void submit(){
        try {
            String sql = "INSERT INTO purchases (product_id,supplier_id,purchase_rate,quantity) VALUES (?,?,?,?)";
            Connection conn = DbConnect.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            
             selectedProduct = products.getSelectionModel().getSelectedItem();
             String[] selectedProd= selectedProduct.split("\\s");
             String s1 = selectedProd[0];
              selectedSupplier = suppliers.getSelectionModel().getSelectedItem();
             String[] selectedSuppliers= selectedSupplier.split("\\s");
             String s2 = selectedSuppliers[0];
             ps.setString(1,s1);
             ps.setString(2,s2);
             ps.setInt(3,Integer.parseInt(purchaserate.getText()));
             ps.setString(4,quantity.getText());
             ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(AddPurchaseController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}





