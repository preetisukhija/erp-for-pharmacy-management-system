/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

/**
 * FXML Controller class
 *
 * @author preeti
 */
public class AddCategoryController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    
    
    @FXML
    private Pane form;

    @FXML
    private TextField addname;

    @FXML
    private Button submit;

    public void display(){
      submit.setOnAction(e->{
          try {
              String categoryName = " ";
              categoryName = addname.getText();
              System.out.println(categoryName);
              Connection conn = DbConnect.getConnection();
              String sql = "INSERT into category (name)" + "values(?)";
              PreparedStatement ps = conn.prepareStatement(sql);
              ps.setString(1,categoryName);
              ps.execute();
          } catch (SQLException ex) {
              Logger.getLogger(AddCategoryController.class.getName()).log(Level.SEVERE, null, ex);
          }
            
        });
}

        @Override
    public void initialize(URL url, ResourceBundle rb) {
      
    }       
}

