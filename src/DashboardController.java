/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Label;


/**
 * FXML Controller class
 *
 * @author preeti
 * 
 * 
 */
   

public class DashboardController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Label lblSql;
    
    @FXML
    private Label lblSql2;

    @FXML
    private Label lblSql3;

    @FXML
    private Label lblSql4;    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
        try {
            Connection conn = DbConnect.getConnection();
            PreparedStatement ps = null;
            String sql = "SELECT COUNT(id) as count FROM suppliers";
            String sql2 = "SELECT COUNT(id) as count2 FROM customers";
            String sql3 = "SELECT COUNT(id) as count3 FROM products";
            String sql4 = "SELECT COUNT(id) as count4 FROM employees";            
            
            ResultSet rs = DbConnect.execute(sql);
            rs.next();
       
            ResultSet rs2 = DbConnect.execute(sql2);
            rs2.next();
            
            ResultSet rs3 = DbConnect.execute(sql3);
            rs3.next();
            
            ResultSet rs4 = DbConnect.execute(sql4);
            rs4.next();
            
            lblSql.setText(rs.getString("count") + " Suppliers");
            lblSql2.setText(rs2.getString("count2") + " Customers");
            lblSql3.setText(rs3.getString("count3") + " Products");
            lblSql4.setText(rs4.getString("count4") + " Employees");
            
        } catch (SQLException ex) {
            Logger.getLogger(DashboardController.class.getName()).log(Level.SEVERE, null, ex);
        }
     }    
    
}
