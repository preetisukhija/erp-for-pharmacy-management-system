/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;

/**
 * FXML Controller class
 *
 * @author preeti
 */
public class AddSupplierController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private AnchorPane scrollablePane;

    @FXML
    private Pane pane;

    @FXML
    private TextField lnametxt;

    @FXML
    private TextField fnametxt;

    @FXML
    private Button submit;

    @FXML
    private TextField gstnotxt;

    @FXML
    private TextField phonenumbertxt;

    @FXML
    private TextField emailidtxt;

    @FXML
    private TextField blocknotxt;

    @FXML
    private TextField streetnotxt;

    @FXML
    private TextField citytxt;

    @FXML
    private TextField pincodetxt;

    @FXML
    private TextField statetxt;

    @FXML
    private TextField countrytxt;

    @FXML
    private TextField towntxt;

    @FXML
    private TextField companyNametxt;
    
    ValidationSupport validationsupport = new ValidationSupport();
    @Override
    public void initialize(URL url, ResourceBundle rb) {
//    gendertxt.getItems().removeAll(gendertxt.getItems());
//    gendertxt.getItems().addAll("Male", "Female");
    
    
    validationsupport.registerValidator(fnametxt, Validator.createEmptyValidator("Text is required"));
    validationsupport.registerValidator(lnametxt, Validator.createEmptyValidator("Text is required"));
    validationsupport.registerValidator(gstnotxt, Validator.createEmptyValidator("Text is required"));
    validationsupport.registerValidator(phonenumbertxt, Validator.createEmptyValidator("Text is required"));
    validationsupport.registerValidator(emailidtxt, Validator.createEmptyValidator("Text is required"));
    validationsupport.registerValidator(blocknotxt, Validator.createEmptyValidator("Text is required"));
    validationsupport.registerValidator(streetnotxt, Validator.createEmptyValidator("Text is required"));
    validationsupport.registerValidator(statetxt, Validator.createEmptyValidator("Text is required"));
    validationsupport.registerValidator(countrytxt, Validator.createEmptyValidator("Text is required"));
    validationsupport.registerValidator(towntxt, Validator.createEmptyValidator("Text is required"));
    validationsupport.registerValidator(pincodetxt, Validator.createEmptyValidator("Text is required"));
    validationsupport.registerValidator(citytxt, Validator.createEmptyValidator("Text is required"));
    validationsupport.registerValidator(companyNametxt, Validator.createEmptyValidator("Text is required"));   
    }   
    
      public void display(){
      submit.setOnAction(e->{
          try {
              Connection conn = DbConnect.getConnection();
              String sql = "INSERT into suppliers (first_name,last_name,gst_no,email_id,phone_no,company_name)" + "values(?,?,?,?,?,?)";
              String sql1 = "INSERT INTO address (block_no,street,city,pincode,state,country,town)"+"values(?,?,?,?,?,?,?)";
              String sql2 = "select id from address";
              String sql3 = "select id from suppliers";
              String sql4 = "INSERT into address_supplier (address_id,supplier_id) values (?,?)";
              
              PreparedStatement ps = conn.prepareStatement(sql);
              PreparedStatement ps1 = conn.prepareStatement(sql1);
              PreparedStatement ps2 = conn.prepareStatement(sql2);
              PreparedStatement ps3 = conn.prepareStatement(sql3);
              PreparedStatement ps4 = conn.prepareStatement(sql4);
              
              ps.setString(1,fnametxt.getText());
              ps.setString(2,lnametxt.getText());
              ps.setString(3,gstnotxt.getText());
              ps.setString(4,emailidtxt.getText());
              ps.setString(5,phonenumbertxt.getText());
              ps.setString(6,companyNametxt.getText());
              
              ps.execute();
              
              ps1.setString(1,blocknotxt.getText());
              ps1.setString(2,streetnotxt.getText());
              ps1.setString(3,citytxt.getText());
              ps1.setString(4,pincodetxt.getText());
              ps1.setString(5,statetxt.getText());
              ps1.setString(6,countrytxt.getText());
              ps1.setString(7,towntxt.getText());
                
              ps1.execute();
              ResultSet rs = ps2.executeQuery();
              rs.last();
              ResultSet rs1 = ps3.executeQuery();
              rs1.last();
              
              int addressId = rs.getInt("id");
              int customerId = rs1.getInt("id");
              ps4.setInt(1,addressId);
              ps4.setInt(2,customerId);
              ps4.execute();
              
          } catch (SQLException ex) { 
              Logger.getLogger(AddCustomerController.class.getName()).log(Level.SEVERE, null, ex);
          }
          try {
           FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Dashboard.fxml"));
           Parent  root1 = (Parent) fxmlLoader.load();
           Stage stage = new Stage();
           stage.setScene(new Scene(root1));  
           stage.show();
          } catch (IOException ex) {
              Logger.getLogger(AddCustomerController.class.getName()).log(Level.SEVERE, null, ex);
          }            
        });
      }
    }
