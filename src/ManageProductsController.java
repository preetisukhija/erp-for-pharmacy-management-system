import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;


/**
 * FXML Controller class
 *
 * @author Jayesh Karia
 */
public class ManageProductsController implements Initializable {
    
    
@FXML
    private TableView<Products> table;

    @FXML
    private TableColumn<Products, Integer> id;

    @FXML
    private TableColumn<Products, String> productname;

    @FXML
    private TableColumn<Products, Integer> quantity;

    @FXML
    private TableColumn<Products, Integer> sellingrate;

    @FXML
    private TableColumn<Products, Integer> wef;

    @FXML
    private TableColumn<Products, Integer> eoq;

    @FXML
    private TableColumn<Products, Integer> dangerlevel;

    @FXML
    private TableColumn<Products, String> categoryname;

    @FXML
    private TableColumn<Products, String> suppliername;

    @FXML
    private TableColumn<Products, Integer> hsn_code;

    @FXML
    private TableColumn<Products, String> Expiry_date;

    @FXML
    private TableColumn<Products, String> action;
    
    @FXML
    private AnchorPane form;
    
    @FXML
    private Label productNamelbl;

    @FXML
    private Label hsncodelbl;

    @FXML
    private Label categorylbl;

    @FXML
    private ComboBox<String> category;

    @FXML
    private Label supplierslbl2;

    @FXML
    private ComboBox<String> suppliers;

    @FXML
    private Button submit;

    @FXML
    private TextField eoq1;

    @FXML
    private Label eoqlbl;

    @FXML
    private TextField dangerlevel1;

    @FXML
    private Label dangerlbl;

    @FXML
    private TextField quantity1;

    @FXML
    private Label quantitylbl;

    @FXML
    private TextField sellingrate1;

    @FXML
    private Label sellingratelbl;

    @FXML
    private ComboBox<String> hsncode;

    @FXML
    private Label supplierslbl;

    @FXML
    private ComboBox<String> associatedsuppliers;

    @FXML
    private DatePicker datepicker;

     @FXML
    private TextField fetchedproducts;
    
    @FXML
    private Label expirydate;
    
        @FXML
    private TextField hiddenTextField;
        
    private String selectedCategory;
    
    private String selectedProduct;
    
    private String selectedSupplier;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        showProducts();
        form.setVisible(false);
        hiddenTextField.setVisible(false);        
    }
   
    
      public ObservableList<Products> getProductsList(){
          int count = 0;
        ObservableList<Products> products = FXCollections.observableArrayList();
         String sql = "SELECT products.id, products.name as product_name, products.Expiry_date,products.hsn_code, products.quantity, products.eoq_level, products.danger_level, category.name as category_name, GROUP_CONCAT(CONCAT(suppliers.first_name,\" \",suppliers.last_name) "
                 + "SEPARATOR ' | ') as supplier_name, products_selling_rate.selling_rate, products_selling_rate.with_effect_from "
                 + "FROM products_selling_rate  INNER JOIN (SELECT product_id, MAX(with_effect_from) as wef FROM (SELECT product_id, with_effect_from FROM products_selling_rate "
                 + "WHERE with_effect_from<=CURRENT_TIMESTAMP) as temp GROUP BY temp.product_id) as max_date_table ON max_date_table.product_id = products_selling_rate.product_id "
                 + "AND products_selling_rate.with_effect_from = max_date_table.wef INNER JOIN products ON products.id = products_selling_rate.product_id INNER JOIN category ON category.id = products.category_id INNER JOIN product_supplier ON product_supplier.product_id = products.id "
                 + "INNER JOIN suppliers ON suppliers.id = product_supplier.supplier_id "
                 + "WHERE products.deleted = 0 GROUP BY products.id";
   
            try {
                /*
                    boolean execute -> returns whether query has affected any rows or not. (Don't use this)
                    int executeUpdate - for insert,update,delete
                    ResultSet executeQuery - for select bcoz it returns a resultSet
                */
                ResultSet rs = DbConnect.execute(sql);
                while(rs.next()){
                    
                    products.add(new Products(rs.getInt("id"),rs.getInt("quantity"),rs.getInt("selling_rate"),rs.getInt("eoq_level"),rs.getInt("danger_level"),rs.getInt("hsn_code"),rs.getString("product_name"),rs.getString("Expiry_date"),rs.getString("with_effect_from"),
                    rs.getString("category_name"),rs.getString("supplier_name")));
                } 
            } catch (SQLException ex) {
                Logger.getLogger(ManageProductsController.class.getName()).log(Level.SEVERE, null, ex);
            }
    return products;    
    }
   
    public void dispose() {
        //scrollPane.setVisible(false);
             
             PreparedStatement ps =  null;
             PreparedStatement ps2 =  null;
         try {
             if((!"".equals(category.getValue())) 
                     && (!"".equals(productname.getText())) 
                     && (!"".equals(sellingrate1.getText()))
                     && (!"".equals(hsncode.getValue()))
                     && (!"".equals(associatedsuppliers.getValue())) 
                     && (!"".equals(eoq1.getText()))
                     && (!"".equals(dangerlevel1.getText()))
                     && (!"".equals(quantity1.getText()))
                     && (!"".equals(datepicker.getValue()))
                     )  {
                 
            Connection conn = DbConnect.getConnection();
            java.util.Date dt = new java.util.Date();
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentTime = sdf.format(dt);
            
             
            String sql = "UPDATE products SET name = ?, hsn_code = ?, category_id = ?, eoq_level =?, danger_level = ?, quantity = ?, updated_at = ? where id = ? ";
            String sql1 = "select selling_rate from products_selling_rate where product_id =(select id from products where id =" + hiddenTextField.getText()+")";
            String sql2 = "SELECT id,name from category where id = (select category_id from products where id = " + hiddenTextField.getText()+")";  
 
            ResultSet rs = DbConnect.execute(sql1);
            rs.next();
                    
            ps = conn.prepareStatement(sql);
            ps2 = conn.prepareStatement(sql2);
            
            String selectedCategory = category.getSelectionModel().getSelectedItem();
            String[] ca = selectedCategory.split("\\s");
            String s = ca[0];
            
            ps.setString(1,fetchedproducts.getText());
            ps.setString(2,hsncode.getValue());
            ps.setInt(3,Integer.parseInt(s));
            ps.setString(4,eoq1.getText());
            ps.setString(5,dangerlevel1.getText());
            ps.setString(6, quantity1.getText());
            ps.setString(7, currentTime);
            ps.setString(8, hiddenTextField.getText());
  
            ps.executeUpdate();
//            ps2.executeUpdate();
            form.setVisible(false);
            showProducts();
          }
        } catch (SQLException ex) {
            Logger.getLogger(DbConnect.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }  
    
 
     public void showProducts(){
    
    ObservableList<Products> list = getProductsList();
    
        id.setCellValueFactory(new PropertyValueFactory<>("id"));
        productname.setCellValueFactory(new PropertyValueFactory<>("product_name"));   
        quantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
        sellingrate.setCellValueFactory(new PropertyValueFactory<>("selling_rate"));
        wef.setCellValueFactory(new PropertyValueFactory<>("with_effect_from"));
        eoq.setCellValueFactory(new PropertyValueFactory<>("eoq_level"));
        dangerlevel.setCellValueFactory(new PropertyValueFactory<>("danger_level"));
        categoryname.setCellValueFactory(new PropertyValueFactory<>("category_name"));
        suppliername.setCellValueFactory(new PropertyValueFactory<>("supplier_name"));
        hsn_code.setCellValueFactory(new PropertyValueFactory<>("hsn_code")); 
        Expiry_date.setCellValueFactory(new PropertyValueFactory<>("Expiry_date"));
       
    Callback<TableColumn<Products,String>,TableCell<Products,String>> cellFactory = (param)->{
    
    final TableCell<Products,String> cell = new TableCell<Products,String>(){
        
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            if(empty) {
                setGraphic(null);
                setText(null);
            }
            else {
                Image img = new Image("trash.png");
                ImageView view = new ImageView(img);
                Image img1 = new Image("pen.png");
                ImageView view1 = new ImageView(img1);
                final Button deleteButton = new Button();
                final Button editButton = new Button();
                final HBox pane = new HBox(deleteButton, editButton);
                deleteButton.setStyle("-fx-background-color: transparent;");
                editButton.setStyle("-fx-background-color: transparent;");
                editButton.setGraphic(view1);
                
                deleteButton.setGraphic(view);

                deleteButton.setOnAction(event -> { 
                Products p = getTableView().getItems().get(getIndex());
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setContentText("Are you sure you want to delete " + " " + p.getProduct_name() + " " + " ? ");
                 String sql= "UPDATE products SET deleted = 1 WHERE id = "+ p.getId();
                 Optional<ButtonType> result = alert.showAndWait();
                  if(!result.isPresent()) {
                     // alert is exited, no button has been pressed.
                  }
    
                    else if(result.get() == ButtonType.OK)
                    {
                        //oke button is pressed
                        DbConnect.delete(sql);
                        showProducts();
                    }
     
                    else if(result.get() == ButtonType.CANCEL) {
                        // cancel button is pressed
                    }
                });
                 editButton.setOnAction(event -> {
                try {                       
                     Products p = getTableView().getItems().get(getIndex());
                        //scrollPane.setVisible(true);
                        form.setVisible(true);
                        
                    helper helper = new helper();
                    category.setValue("Choose Category:");
                    associatedsuppliers.setValue("Choose Suppliers:");
                  
                    HashMap<String, String> categoryList = new HashMap<>();
                    categoryList = DbConnect.getCategories();
                    helper.setDataFromArraylistToCombobox(category, categoryList);

                    HashMap<String, String> hsncodeList = new HashMap<>();
                    hsncodeList = DbConnect.getHsn_code();
                    helper.setDataFromArraylistToCombobox(hsncode, hsncodeList);
                    
//                    HashMap<String, String> supplierList = new HashMap<>();
//                    selectedCategory = category.getSelectionModel().getSelectedItem();
//                    String[] words=selectedCategory.split("\\s");
//                    String Selectedcate = words[0];
//                    selectedProduct = hsncode.getSelectionModel().getSelectedItem();
//                    String[] selectedProd = selectedProduct.split("\\s");
//                    String s1 = selectedProd[0];
//                    supplierList = DbConnect.getSuppliersAssociated(Selectedcate, s1);
//                    helper.setDataFromArraylistToCombobox(associatedsuppliers, supplierList);
//                    
                    hiddenTextField.setText(p.getId()+"");        
                    String sql = "SELECT id,name from category where id = (select category_id from products where id = " + hiddenTextField.getText()+")";
                    System.out.println(sql);
                    ResultSet rs = DbConnect.execute(sql);
                    rs.next();

                    int categoryId = 0;
                    String name = "";
                    categoryId = rs.getInt("id");
                    name = rs.getString("name");

                    String c = categoryId + name;
                    category.setValue(categoryId + " " + name); 
                    fetchedproducts.setText(p.getProduct_name());    
                    sellingrate1.setText(p.getSelling_rate()+"");
                    hsncode.setValue(p.getHsn_code()+"");
                    associatedsuppliers.setValue(p.getSupplier_name());
                    eoq1.setText(p.getEoq_level()+"");
                    dangerlevel1.setText(p.getDanger_level() + "");
                    quantity1.setText(p.getQuantity() + "");
                } catch (SQLException ex) {
                    Logger.getLogger(ManageProductsController.class.getName()).log(Level.SEVERE, null, ex);
                }

                });
               
       setGraphic(pane);
       setText(null);
            }
        }
    };
 return cell;
};
    action.setCellFactory(cellFactory);
    table.setItems(list);
  }

}