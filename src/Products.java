/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author preeti
 */
public class Products {
    int id,quantity,selling_rate,eoq_level, danger_level, hsn_code;
    String product_name, Expiry_date, with_effect_from, category_name, supplier_name;

    public Products(int id, int quantity, int selling_rate, int eoq_level, int danger_level, int hsn_code, String product_name, String Expiry_date, String with_effect_from, String category_name, String supplier_name) {
        this.id = id;
        this.quantity = quantity;
        this.selling_rate = selling_rate;
        this.eoq_level = eoq_level;
        this.danger_level = danger_level;
        this.hsn_code = hsn_code;
        this.product_name = product_name;
        this.Expiry_date = Expiry_date;
        this.with_effect_from = with_effect_from;
        this.category_name = category_name;
        this.supplier_name = supplier_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getSelling_rate() {
        return selling_rate;
    }

    public void setSelling_rate(int selling_rate) {
        this.selling_rate = selling_rate;
    }

    public int getEoq_level() {
        return eoq_level;
    }

    public void setEoq_level(int eoq_level) {
        this.eoq_level = eoq_level;
    }

    public int getDanger_level() {
        return danger_level;
    }

    public void setDanger_level(int danger_level) {
        this.danger_level = danger_level;
    }

    public int getHsn_code() {
        return hsn_code;
    }

    public void setHsn_code(int hsn_code) {
        this.hsn_code = hsn_code;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getExpiry_date() {
        return Expiry_date;
    }

    public void setExpiry_date(String Expiry_date) {
        this.Expiry_date = Expiry_date;
    }

    public String getWith_effect_from() {
        return with_effect_from;
    }

    public void setWith_effect_from(String with_effect_from) {
        this.with_effect_from = with_effect_from;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getSupplier_name() {
        return supplier_name;
    }

    public void setSupplier_name(String supplier_name) {
        this.supplier_name = supplier_name;
    }
    
    
}
