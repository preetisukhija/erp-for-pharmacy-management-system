/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import javafx.fxml.Initializable;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author preeti
 */
public class ManageCategoryController implements Initializable {

    /**
     * Initializes the controller class.
     */

//    @FXML
//    private AnchorPane search;
//
//    @FXML
//    private TextField search_text;

    @FXML
    private TableView<CategoryPanel> table;

    @FXML
    private TableColumn<CategoryPanel, Integer> id;

    @FXML
    private TableColumn<CategoryPanel, String> name;

    @FXML
    private TableColumn<CategoryPanel, String> action;
    
    @FXML
    private Pane form;

    @FXML
    private TextField udatedname;

    @FXML
    private Button submit;
    
    @FXML
    private TextField hiddenTF;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        showCategroys();
        form.setVisible(false);
        hiddenTF.setVisible(false);
    }
  
    public ObservableList<CategoryPanel> getCategoryList(){
        ObservableList<CategoryPanel> categorys = FXCollections.observableArrayList();
         String sql = "SELECT id,name from category where deleted = 0"; 
         ResultSet rs = DbConnect.execute(sql);
            try {
                while(rs.next()){
                categorys.add(new CategoryPanel(rs.getInt("id"),rs.getString("name")));
                } 
            } catch (SQLException ex) {   
                Logger.getLogger(ManageCategoryController.class.getName()).log(Level.SEVERE, null, ex);
    }   
    return categorys;    
 }   
     public void showCategroys(){
  
         
    ObservableList<CategoryPanel> list = getCategoryList();
    
    id.setCellValueFactory(new PropertyValueFactory<>("id"));
    name.setCellValueFactory(new PropertyValueFactory<>("name"));


//Addong buttons in each cell
Callback<TableColumn<CategoryPanel,String>,TableCell<CategoryPanel,String>> cellFactory = (param)->{
    
    final TableCell<CategoryPanel,String> cell = new TableCell<CategoryPanel,String>(){
        
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            if(empty) {
                setGraphic(null);
                setText(null);
            }
    else {
                Image img = new Image("trash.png");
                ImageView view = new ImageView(img);
                Image img1 = new Image("pen.png");
                ImageView view1 = new ImageView(img1);
                
                final Button deleteButton = new Button();
                final Button editButton = new Button();
                final HBox pane = new HBox(deleteButton, editButton);
                deleteButton.setStyle("-fx-background-color: transparent;");
                editButton.setStyle("-fx-background-color: transparent;");
                editButton.setGraphic(view1);
                         
                
                deleteButton.setGraphic(view);
                deleteButton.setOnAction(event -> { 
                CategoryPanel c = getTableView().getItems().get(getIndex());
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setContentText("Are you sure you want to delete " + " " + c.getName() + " " + "record ? ");
                 String sql= "UPDATE category SET deleted = 1 WHERE id = "+ c.getId();
                 Optional<ButtonType> result = alert.showAndWait();
                  if(!result.isPresent()) {
                     // alert is exited, no button has been pressed.
                  }
    
                    else if(result.get() == ButtonType.OK)
                    {
                        //oke button is pressed
                        DbConnect.delete(sql);
                        showCategroys();
                    }
     
                    else if(result.get() == ButtonType.CANCEL) {
                        // cancel button is pressed
                    }
                });
                editButton.setOnAction(event -> { 
                 
                        CategoryPanel c = getTableView().getItems().get(getIndex());
                        form.setVisible(true);
//                      horizontalPane.setVisible(false);
                        udatedname.setText(c.getName());
                        hiddenTF.setText(c.getId()+"");
                });
                setGraphic(pane);
                setText(null);
            }
        }
    };
 return cell;
};
action.setCellFactory(cellFactory);
table.setItems(list);
    }    
       
    public void dispose(){
        form.setVisible(false);
     
        PreparedStatement ps =  null;
         try {
             if((!"".equals(udatedname.getText()))) {
                 
            Connection conn = DbConnect.getConnection();
            String updatedCategory = "Preeti";
            String updatedId = hiddenTF.getText();
             
             java.util.Date dt = new java.util.Date();
             java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
             String currentTime = sdf.format(dt);
             
             String sql = "UPDATE category set name= ?, updated_at = ? where id = ? ";
             ps = conn.prepareStatement(sql);
             ps.setString(1,udatedname.getText());
             ps.setString(3,hiddenTF.getText());
             ps.setString(2, currentTime);
             
             
            ps.executeUpdate();
            showCategroys();
          }
        } catch (SQLException ex) {
            Logger.getLogger(DbConnect.class.getName()).log(Level.SEVERE, null, ex);
        }    
     }  
}

