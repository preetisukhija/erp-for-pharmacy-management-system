/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import javafx.fxml.Initializable;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author preeti
 */
public class ManageEmployeesController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private TableView<Employee> table;

    @FXML
    private TableColumn<Employee, Integer> id_col;

    @FXML
    private TableColumn<Employee, String> name_col;

    @FXML
    private TableColumn<Employee, String> phoneNo_col;

    @FXML
    private TableColumn<Employee, String> email_col;

    @FXML
    private TableColumn<Employee, String> address_col;

    @FXML
    private TableColumn<Employee, String> gender_col;

    @FXML
    private TableColumn<Employee, String> action_col;

    @FXML
    private TextField search;
    
    
    //variables for the edit panel
    @FXML
    private AnchorPane form;

    @FXML
    private Pane pane;

    @FXML
    private TextField lnametxt;

    @FXML
    private TextField fnametxt;

    @FXML
    private Button submit;

    @FXML
    private TextField phonenumbertxt;

    @FXML
    private TextField emailidtxt;

    @FXML
    private ComboBox<String> gendertxt;

    @FXML
    private TextField blocknotxt;

    @FXML
    private TextField streetnotxt;

    @FXML
    private TextField citytxt;

    @FXML
    private TextField pincodetxt;

    @FXML
    private TextField statetxt;

    @FXML
    private TextField countrytxt;

    @FXML
    private TextField towntxt;

    @FXML
    private TextField hiddenTextField;
    
    
   @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        gendertxt.getItems().removeAll(gendertxt.getItems());
        gendertxt.getItems().addAll("Male", "Female");
        showEmployees();
        form.setVisible(false);
        hiddenTextField.setVisible(false);
    
    }   
    
    
    public ObservableList<Employee> getEmployeeList(){
        ObservableList<Employee> employees = FXCollections.observableArrayList();
         String sql = "SELECT e.id, concat(e.first_name,\" \",e.last_name) as full_name, e.email_id, e.phone_no, e.gender, concat(a.block_no,\";\",a.street,\";\",a.city,\";\",a.pincode,\";\",a.state,\";\",a.country,\";\",a.town) as full_address\n" +
                 "from employees as e \n" +
                 "INNER JOIN address a \n" + 
                 "on e.address_id = a.id \n" +
                  "WHERE e.deleted = 0 AND a.deleted = 0;";
         
         ResultSet rs = DbConnect.execute(sql);
            try {
                while(rs.next()){
                employees.add(new Employee(rs.getString("full_name"),rs.getString("email_id"),rs.getString("full_address"), rs.getString("phone_no"), rs.getString("gender"), rs.getInt("id")));
                } 
            } catch (SQLException ex) {   
                Logger.getLogger(ManageEmployeesController.class.getName()).log(Level.SEVERE, null, ex);
    }   
    return employees;    
 }
    public void dispose() {
        //scrollPane.setVisible(false);
             
             PreparedStatement ps =  null;
             PreparedStatement ps2 =  null;
         try {
             if((!"".equals(fnametxt.getText())) 
                     && (!"".equals(lnametxt.getText())) 
                     && (!"".equals(emailidtxt.getText())) 
                     && (!"".equals(phonenumbertxt.getText()))
                     && (!"".equals(gendertxt.getValue()))
                     && (!"".equals(blocknotxt.getText()))
                     && (!"".equals(streetnotxt.getText()))
                     && (!"".equals(citytxt.getText()))
                     && (!"".equals(statetxt.getText()))
                     && (!"".equals(countrytxt.getText()))
                     && (!"".equals(towntxt.getText()))
                     )  {
                 
            Connection conn = DbConnect.getConnection();
            java.util.Date dt = new java.util.Date();
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentTime = sdf.format(dt);
            
             
            String sql = "UPDATE employees SET first_name = ?, last_name = ?, email_id = ?, phone_no = ?, gender = ?, updated_at = ? where id = ? ";
            String sql1 = "SELECT address_id from employees where id ="+ hiddenTextField.getText();
            ResultSet rs = DbConnect.execute(sql1);
            rs.next();
            
            int addressId = rs.getInt("address_id");
            System.out.println(addressId);
            String sql2 = "UPDATE address SET block_no = ?, street = ?,city = ?, pincode=?, state=?, country=?, town=?, updated_at = ? where id= ?";
            
            
            ps = conn.prepareStatement(sql);
            ps2 = conn.prepareStatement(sql2);
            
            ps.setString(1,fnametxt.getText());
            ps.setString(2,lnametxt.getText());
            ps.setString(3,emailidtxt.getText());
            ps.setString(4,phonenumbertxt.getText());
            ps.setString(5,gendertxt.getValue());
            ps.setString(6, currentTime);
            ps.setString(7, hiddenTextField.getText());
            
            ps2.setString(1,blocknotxt.getText());
            ps2.setString(2,streetnotxt.getText());
            ps2.setString(3,citytxt.getText());
            ps2.setString(4,pincodetxt.getText());
            ps2.setString(5,statetxt.getText());
            ps2.setString(6,countrytxt.getText());
            ps2.setString(7,towntxt.getText());
            ps2.setString(8,currentTime);
            ps2.setInt(9,addressId);
            
            
            ps.executeUpdate();
            ps2.executeUpdate();
            
            showEmployees();
          }
        } catch (SQLException ex) {
            Logger.getLogger(DbConnect.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    public void showEmployees(){
  
         
    ObservableList<Employee> list = getEmployeeList();
    
    id_col.setCellValueFactory(new PropertyValueFactory<>("id"));
    name_col.setCellValueFactory(new PropertyValueFactory<>("name"));
    phoneNo_col.setCellValueFactory(new PropertyValueFactory<>("phoneNo"));
    email_col.setCellValueFactory(new PropertyValueFactory<>("email"));
    gender_col.setCellValueFactory(new PropertyValueFactory<>("gender"));
    address_col.setCellValueFactory(new PropertyValueFactory<>("address"));

//Addong buttons in each cell
Callback<TableColumn<Employee,String>,TableCell<Employee,String>> cellFactory = (param)->{
    
    final TableCell<Employee,String> cell = new TableCell<Employee,String>(){
        
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            if(empty) {
                setGraphic(null);
                setText(null);
            }
    else {
                Image img = new Image("trash.png");
                ImageView view = new ImageView(img);
                Image img1 = new Image("pen.png");
                ImageView view1 = new ImageView(img1);
                
                final Button deleteButton = new Button();
                final Button editButton = new Button();
                final HBox pane = new HBox(deleteButton, editButton);
                deleteButton.setStyle("-fx-background-color: transparent;");
                editButton.setStyle("-fx-background-color: transparent;");
                editButton.setGraphic(view1);
                         
                
                deleteButton.setGraphic(view);
                deleteButton.setOnAction(event -> { 
                Employee e = getTableView().getItems().get(getIndex());
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setContentText("Are you sure you want to delete " + " " + e.getName() + " " + "record ? ");
                 String sql= "UPDATE employees SET deleted = 1 WHERE id = "+ e.getId();
                 Optional<ButtonType> result = alert.showAndWait();
                  if(!result.isPresent()) {
                     // alert is exited, no button has been pressed.
                  }
    
                    else if(result.get() == ButtonType.OK)
                    {
                        //oke button is pressed
                        DbConnect.delete(sql);
                        showEmployees();
                    }
     
                    else if(result.get() == ButtonType.CANCEL) {
                        // cancel button is pressed
                    }
                });
                editButton.setOnAction(event -> { 
                 
                        Employee e = getTableView().getItems().get(getIndex());
                        //scrollPane.setVisible(true);
                        form.setVisible(true);
                        String employeeName = e.getName();
                        String[] words = employeeName.split("\\s");
                            fnametxt.setText(words[0]);
                            lnametxt.setText(words[1]);
                        emailidtxt.setText(e.getEmail());
                        gendertxt.setValue(e.getGender());
                        hiddenTextField.setText(e.getId()+"");
                        phonenumbertxt.setText(e.getPhoneNo()); 
                        
                        String address = e.getAddress();
                        String addresswords[] = address.split(";");
                        
                        System.out.println(addresswords);
                        blocknotxt.setText(addresswords[0]);
                        streetnotxt.setText(addresswords[1]);
                        citytxt.setText(addresswords[2]);
                        pincodetxt.setText(addresswords[3]);
                        statetxt.setText(addresswords[4]);
                        countrytxt.setText(addresswords[5]);
                        towntxt.setText(addresswords[6]);  
                        
    });
   

                setGraphic(pane);
                setText(null);
            }
        }
    };
 return cell;
};
action_col.setCellFactory(cellFactory);
table.setItems(list);
    }    
}
