import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class InvoiceController implements Initializable {

    @FXML
    private TableView<Invoice> table;
    @FXML
    private TableColumn<Invoice,Integer> id;
    @FXML
    private TableColumn<Invoice, String> product;

    @FXML
    private TableColumn<Invoice, String> specifications;

    @FXML
    private TableColumn<Invoice, Integer> price;

    @FXML
    private TableColumn<Invoice, Integer> quantity;

    @FXML
    private TableColumn<Invoice, Integer> discount;

    @FXML
    private TableColumn<Invoice, Double> total;

    @FXML
    private Label phoneno;

    

    @FXML
    private Label date;

    @FXML
    private Label name;

    @FXML
    private Label email;

    @FXML
    private Label address;

    @FXML
    private Label gstno;

    @FXML
    private Label finaltotal;
    
    @FXML
    private Label invoiceId;
    
    private double finalRate = 0;
    private int selectedInvoiceId;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            java.util.Date dt = new java.util.Date();
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
            String currentTime = sdf.format(dt);
            date.setText(currentTime);
            String sql = "select concat(customers.first_name, \" \",customers.last_name)as customer_name,customers.gst_no,customers.phone_no,"
                    + "customers.email_id,customers.gender, "
                    + "CONCAT(address.block_no, \" ,\",address.street, \" ,\",address.city, \" ,\",address.pincode, \" ,\","
                    + "address.state, \" ,\",address.town, \" ,\",address.country) "
                    + "as customer_address "
                    + ",sales.quantity,sales.discount,products.name,products.specification,products_selling_rate.selling_rate from sales "
                    + "inner join products on sales.product_id = products.id "
                    + "inner join products_selling_rate on products.id = products_selling_rate.product_id "
                    + "inner join invoice on invoice.id = sales.invoice_id inner join customers on customers.id = invoice.customer_id "
                    + "INNER join address on address.id = (select address_customer.address_id from address_customer "
                    + "where address_customer.customer_id = invoice.customer_id) where sales.invoice_id = ?";
            String sql2 = "SELECT id from invoice";
            ResultSet rs = DbConnect.execute(sql2);
            rs.last();
            
            selectedInvoiceId = rs.getInt("id");
          
            Connection conn = DbConnect.getConnection();
            PreparedStatement ps1 = conn.prepareStatement(sql);
            ps1.setInt(1,selectedInvoiceId);
            ResultSet rs1 = ps1.executeQuery();
            
            while(rs1.next()){
            name.setText(rs1.getString("customer_name"));
            phoneno.setText(rs1.getString("phone_no"));
            email.setText(rs1.getString("email_id"));
            gstno.setText(rs1.getString("gst_no"));
            address.setText(rs1.getString("customer_address"));
            }
            showInvoiceData();
        } catch (SQLException ex) {
            Logger.getLogger(InvoiceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }    
    
      public ObservableList<Invoice> getInvoiceDataList(){
          
        ObservableList<Invoice> invoice = FXCollections.observableArrayList();
        try {
            
         String sql = "select concat(customers.first_name,customers.last_name)as customer_name,customers.gst_no,customers.phone_no,customers.email_id,customers.gender, CONCAT(address.block_no,address.street) as customer_address ,sales.quantity,sales.discount,products.name as product_name,products.specification,products_selling_rate.selling_rate from sales inner join products on sales.product_id = products.id inner join products_selling_rate on products.id = products_selling_rate.product_id inner join invoice on invoice.id = sales.invoice_id inner join customers on customers.id = invoice.customer_id INNER join address on address.id = (select address_customer.address_id from address_customer where address_customer.customer_id = invoice.customer_id) where sales.invoice_id = ?";
            String sql2 = "SELECT id from invoice";
            ResultSet rs = DbConnect.execute(sql2);
            rs.last();
            selectedInvoiceId = rs.getInt("id");
            invoiceId.setText("#" + " " + selectedInvoiceId +"");
            
          
            Connection conn = DbConnect.getConnection();
            
            
            
            PreparedStatement ps1 = conn.prepareStatement(sql);
            ps1.setInt(1,selectedInvoiceId);
            ResultSet rs1 = ps1.executeQuery();
            
            double discountedRate = 0;
                while(rs1.next()){
                    
                    if(!((rs1.getInt("discount")== 0) && (rs1.getInt("selling_rate")== 0)) ) {
                        finalRate = rs1.getInt("quantity") * rs1.getInt("selling_rate");
                        
                         discountedRate  = finalRate * (rs1.getInt("discount"));
                         discountedRate = discountedRate / 100;
                         finalRate = rs1.getInt("selling_rate") * rs1.getInt("quantity") - discountedRate;
                         finaltotal.setText("RS" + " " + finalRate+"");
                    }else {
                        finalRate = rs1.getInt("quantity") * rs1.getInt("selling_rate");
                        finaltotal.setText("RS" + " " + finalRate+"");
                    }
                invoice.add(new Invoice(rs1.getString("product_name"),rs1.getString("specification"),
                rs1.getInt("selling_rate"),rs1.getInt("quantity"),rs1.getInt("discount"), finalRate));
                }
        } catch (SQLException ex) {
            Logger.getLogger(InvoiceController.class.getName()).log(Level.SEVERE, null, ex);
        }
    return invoice;    
 }
    public void showInvoiceData(){
        
    
    ObservableList<Invoice> list = getInvoiceDataList();
    
    product.setCellValueFactory(new PropertyValueFactory<>("product"));
    specifications.setCellValueFactory(new PropertyValueFactory<>("specifications"));
    price.setCellValueFactory(new PropertyValueFactory<>("price"));
    quantity.setCellValueFactory(new PropertyValueFactory<>("quantity"));
    discount.setCellValueFactory(new PropertyValueFactory<>("discount"));
    total.setCellValueFactory(new PropertyValueFactory<>("total"));
    
    
    table.setItems(list);
    }
}
