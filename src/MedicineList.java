
import java.sql.Date;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author preeti
 */
public class MedicineList {
    String medicine_name, date;
        int id, hsn_code,category_id, eoq_level, danger_level, quantity;

    public MedicineList(String medicine_name, String date, int id, int hsn_code, int category_id, int eoq_level, int danger_level, int quantity) {
        this.medicine_name = medicine_name;
        this.date = date;
        this.id = id;
        this.hsn_code = hsn_code;
        this.category_id = category_id;
        this.eoq_level = eoq_level;
        this.danger_level = danger_level;
        this.quantity = quantity;
    }


    public String getMedicine_name() {
        return medicine_name;
    }

    public void setMedicine_name(String medicine_name) {
        this.medicine_name = medicine_name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHsn_code() {
        return hsn_code;
    }

    public void setHsn_code(int hsn_code) {
        this.hsn_code = hsn_code;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public int getEoq_level() {
        return eoq_level;
    }

    public void setEoq_level(int eoq_level) {
        this.eoq_level = eoq_level;
    }

    public int getDanger_level() {
        return danger_level;
    }

    public void setDanger_level(int danger_level) {
        this.danger_level = danger_level;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
