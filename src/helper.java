
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author preeti
 */
public class helper {
    
    public void setDataFromArraylistToCombobox(ComboBox cmb, HashMap hm){
        
        ArrayList arrList = new ArrayList();
        Set<Map.Entry<Integer,String>> s = hm.entrySet();
  
        
        for (Map.Entry<Integer, String> it: s)
        {
            
            arrList.add(it.getKey() + " " +  it.getValue());
            
        } 
         ObservableList categoryList = FXCollections.observableArrayList(arrList);
         cmb.setItems(categoryList);
    }
    
   
    HashMap insertDataInHashmap(ResultSet rs, String col,String id) {
        HashMap hashmap = new HashMap();
        try {
            while(rs.next()) {
                hashmap.put(rs.getInt(id),rs.getString(col));
            }
        } catch (SQLException ex) {
            Logger.getLogger(helper.class.getName()).log(Level.SEVERE, null, ex);
        }
    return hashmap;
    }
    
   void addingStartingItemInCombobox(ComboBox category, ComboBox products){
        category.setValue("Choose Category");
        products.setValue("Choose Products");
    }
   
    ArrayList insertDataInArrayList(ResultSet rs, String col) {
        ArrayList arrlist = new ArrayList<>();
        try {
            while(rs.next()) {
                arrlist.add(rs.getString(col));
            }
        } catch (SQLException ex) {
            Logger.getLogger(helper.class.getName()).log(Level.SEVERE, null, ex);
        }
    return arrlist;
    }
}


