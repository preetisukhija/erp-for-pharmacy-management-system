/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author preeti
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public class DbConnect {
    private static final String CONNECTION_STRING = "jdbc:mysql://localhost:3306/pms";
    private static final String USERNAME ="Preeti";
    private static final String PASSWORD = "Preeti@1234";
    
    
    
    public static Connection getConnection(){
        Connection conn = null;
        
        try{
            conn = DriverManager.getConnection(CONNECTION_STRING,USERNAME,PASSWORD);
            System.out.println(conn);
        }
        catch(SQLException ex){
            
            JOptionPane.showMessageDialog(null,"Conncetion Failed :"+ ex.getMessage());
        }
        return conn;
    }
    
    
    public static ResultSet execute(String sql){
        Connection conn = getConnection();
         PreparedStatement ps = null;
         ResultSet rs = null;
        try {
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DbConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }
    
    public static void delete(String sql){
        PreparedStatement ps =  null;
        ResultSet rs = null;
         try {
            Connection conn = getConnection();
            ps = conn.prepareStatement(sql);
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(DbConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args){
        Connection conn = getConnection();
    }    
    
    public static HashMap getCategories(){

   HashMap<Integer,String> hm = new HashMap<>();
        try{
            String sql = "select id, name from category where deleted = 0";
   
            ArrayList arrList = new ArrayList();
            ResultSet rs = DbConnect.execute(sql);
            
            while(rs.next()){
                hm.put(rs.getInt("id"),rs.getString("name"));
            }
            
        }catch (SQLException ex) {    
            Logger.getLogger(AddProductsController.class.getName()).log(Level.SEVERE, null, ex);
        }   
        return hm;
    }
    
    public static HashMap getHsn_code(){
        HashMap<Integer,String> hm = new HashMap<>();
        try{
            String sql = "select id, hsn_code from products where deleted = 0";
   
            ArrayList arrList = new ArrayList();
            ResultSet rs = DbConnect.execute(sql);
            
            while(rs.next()){
                hm.put(rs.getInt("id"),rs.getString("hsn_code"));
            }
        }catch (SQLException ex) {    
            Logger.getLogger(AddProductsController.class.getName()).log(Level.SEVERE, null, ex);
        }   
        return hm;
    }
    
    public static HashMap getSuppliers(String category){
        HashMap<Integer,String> hm = new HashMap<>();
        PreparedStatement ps = null;
           ResultSet rs = null;
           helper h = new helper();
        try{
            String sql = "SELECT s.id, concat(s.first_name, s.last_name) AS name from suppliers s \n" +
                     "INNER JOIN product_supplier ps \n" +
                     "ON ps.supplier_id = s.id \n" +
                     "INNER JOIN products p \n" +
                     "on ps.product_id = p.id where p.category_id = ?;";
   
            ArrayList arrList = new ArrayList();
           
            
            Connection conn = getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, category);
            rs = ps.executeQuery();
            
        }catch (SQLException ex) {
            Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
        HashMap hashmap = h.insertDataInHashmap(rs,"name","id");
        return hashmap;
        
    }
    
      public static HashMap getSuppliersNotAssociated(String category, String products){
      

        String sql = "SELECT s.id, concat(s.first_name,s.last_name) as supplier_name,p.name\n" +
        "FROM suppliers s\n" +
        "INNER JOIN \n" +
        "product_supplier ps \n" +
        "ON ps.supplier_id = s.id\n" +
        "INNER JOIN products p\n" +
        "ON ps.product_id = p.id where p.category_id = ?"
        + "and ps.supplier_id NOT IN"
        + "(SELECT product_supplier.supplier_id from product_supplier where product_supplier.product_id "
        + "IN (select id from products where id = ? and category_id = ?))";

       PreparedStatement ps = null;
       ResultSet rs = null;
       helper helper = new helper();
          try{
         Connection conn = getConnection();
          ps = conn.prepareStatement(sql);
          ps.setString(1, category);
          ps.setString(2, products);
          ps.setString(3, category);
          rs = ps.executeQuery();
        } catch (SQLException ex) {
        Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
        HashMap hashmap = helper.insertDataInHashmap(rs,"supplier_name","id");
        return hashmap;
        
    }
   
     public static HashMap getSuppliersAssociated(String category, String products){
         
        String sql ="select suppliers.id,concat(suppliers.first_name,\" \",suppliers.last_name)as supplier_name from suppliers where suppliers.id IN(SELECT product_supplier.supplier_id from product_supplier where product_supplier.product_id IN (select id from products where id = ? and category_id = ?)) ORDER BY id";

       PreparedStatement ps = null;
       ResultSet rs = null;
       helper helper = new helper();
          try{
         Connection conn = getConnection();
          ps = conn.prepareStatement(sql);
          
          ps.setString(1, products);
          ps.setString(2, category);
          rs = ps.executeQuery();
        } catch (SQLException ex) {
        Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
        HashMap hashmap = helper.insertDataInHashmap(rs,"supplier_name","id");
        return hashmap;
        
    }
   
    public static HashMap getSuppliersAccordingToCategory(String cate,String id){
       
       String sql = "SELECT ps.id as id, p.category_id,p.name,concat(s.first_name, \" \", s.last_name) as supplier_name\n" +
                    "FROM suppliers s\n" +
                    "INNER JOIN product_supplier ps\n" +
                    "ON s.id = ps.supplier_id\n" +
                    "INNER JOIN products p\n" +
                    "ON p.id = ps.product_id\n" +
                    "where p.category_id = ? and p.id = ?";
       
       PreparedStatement ps = null;
       ResultSet rs = null;
       
       helper helper = new helper();
          try{
         Connection conn = getConnection();
         
          ps = conn.prepareStatement(sql);
          ps.setString(1,cate);
         ps.setString(2,id);
          rs = ps.executeQuery();
          }
         catch (SQLException ex) {
        Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
         
        HashMap hashmap = helper.insertDataInHashmap(rs, "supplier_name", "id");
        return hashmap;
        
    }
    
        public static HashMap getProductsAccorgingToCategory(String category){
         
         String sql = "select id,name from products where category_id = (select id from category where id = ?)";
        
         PreparedStatement ps = null;
         ResultSet rs = null;
         helper helper = new helper();
         try{
            Connection conn = DbConnect.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1,category);
            rs = ps.executeQuery();
           }catch (SQLException ex) {
                Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
            }
            HashMap hashmap = helper.insertDataInHashmap(rs,"name","id");
        return hashmap;
   
        }
        
       public static int getSellingRate(String category,String product){
            String sql = "select selling_rate from products_selling_rate ps  \n" +
            "INNER JOIN products p\n" +
            "ON p.id = ps.product_id\n" +
            "where category_id = (select id from category where id = ? ) AND p.id=?";
            
           int sellingRate = 0;
           PreparedStatement ps = null;
          ResultSet rs = null;
         try{
            Connection conn = DbConnect.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1,category);
            ps.setString(2, product);
            rs = ps.executeQuery();
           rs.next();
           sellingRate = rs.getInt("selling_rate");
          return sellingRate;  
           }catch (SQLException ex) {
                Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
            }
       return 0;
        }
        
        public static int getPurchaseRate(String category, String product){
            String sql = "SELECT p.name,p.category_id,pu.purchase_rate FROM products p INNER JOIN purchases pu ON pu.product_id = p.id where pu.product_id = ? AND p.category_id=?";
            
           int purchaseRate = 0;
           PreparedStatement ps = null;
           ResultSet rs = null;
         try{
            Connection conn = DbConnect.getConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1,category);
            ps.setString(2, product);
           
            rs = ps.executeQuery();
            if(rs.next()) {
                rs.last();
                purchaseRate = rs.getInt("purchase_rate");
                return purchaseRate;  
            }else {
                return 0;
            }
        }catch (SQLException ex) {
                Logger.getLogger(ManageCustomerController.class.getName()).log(Level.SEVERE, null, ex);
            }
       return -1;
        }
    }

